import Vue from 'vue';
import Vuex from 'vuex';

import TicTacToe from './models/TicTacToe';
import { Player, Location } from './models/types';

Vue.use(Vuex);

type State = {
  game: TicTacToe,
};

const initialState: State = {
  game: new TicTacToe(),
};

export default new Vuex.Store({
  state: initialState,
  mutations: {
    makeMove(state: State,
      { location, player }: { location: Location, player: Player }) {
      state.game.setLocation(location, player);
    },
  },
});
