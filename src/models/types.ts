enum Player {
    X = -1,
    O = 1,
}

type Location = { row: number, col: number };

export { Player, Location };
