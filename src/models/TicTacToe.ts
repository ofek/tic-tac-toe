import { Location, Player } from './types';

export default class TicTacToe {
  rows = 3;

  cols = 3;

  winCondition = 3;

  openLocations = this.rows * this.cols;

  // Array.prototype.map ignores uninitialized values, so we .fill(0)
  board: Array<Array<Player | null>> = new Array(this.rows).fill(0)
    .map(row => new Array(this.cols).fill(null));

  // initial row/col score
  rowScore: Array<number> = new Array(this.rows).fill(0);

  colScore: Array<number> = new Array(this.cols).fill(0);

  // initial diagonals score
  maindScore: number = 0;

  antidScore: number = 0;

  winner: undefined | null | Player = undefined;

  getLocation({ row, col }: Location) {
    return this.board[row][col];
  }

  setLocation(location: Location, player: Player) {
    const { row, col } = location;

    if (this.winner !== undefined) {
      return;
    }

    if (this.board[row][col] === null) {
      this.board[row][col] = player;

      // increment row/col scores
      this.rowScore[row] += player;
      this.colScore[col] += player;


      // increment main diagonal score
      if (row === col) {
        this.maindScore += player;
      }

      // increment anti diagonal score
      if ((row === 1 && col === 1) || (row === 0 && col === 2) || (row === 2 && col === 0)) {
        this.antidScore += player;
      }

      this.openLocations -= 1;
    } else {
      throw new Error('Location already occupied');
    }

    if (Math.abs(this.rowScore[row]) >= this.winCondition
      || Math.abs(this.colScore[col]) >= this.winCondition
      || Math.abs(this.maindScore) >= this.winCondition
      || Math.abs(this.antidScore) >= this.winCondition) {
      this.winner = player;
    } else if (this.openLocations === 0) {
      this.winner = null;
    }
  }
}
